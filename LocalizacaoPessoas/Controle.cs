﻿using System;
using System.Collections.Generic;

namespace LocalizacaoPessoas
{
    class Controle
    {        
        List<Pessoa> Pessoas;

       public Controle()
        {
            this.Pessoas = new List<Pessoa>();
            MenuPrincipal();
        }

        void OpcaoInvalida()
        {
            Console.Clear();
            Console.WriteLine("Opção inválida. Pressione qualquer tecla para continuar...");
            Console.ReadLine();
            MenuPrincipal();
        }
        
        void MenuPrincipal()
        {
            Console.Clear();

            string[] mensagens = new string[4];

            mensagens[0] = "1) Listar amigos.";
            mensagens[1] = "2) Listar amigos próximos.";
            mensagens[2] = "3) Cadastrar amigo.";
            mensagens[3] = "4) Sair.";

            for (int cont = 0; cont < 4; cont++)
                Console.WriteLine(mensagens[cont]);

            
            object opcao = Console.ReadLine();
            LerOpcao(opcao);            
        }
        
        void LerOpcao(Object opcao)
        {
            try
            {
                    
                int item = Convert.ToInt32(opcao);

                switch (item)
                    {
                    case 1:
                        {
                            ListarAmigos();
                            break;
                        }
                    case 2:
                        {
                            ListarAmigosProximos();
                            break;
                        }
                    case 3:
                        {
                            CadastrarAmigo();
                            break;
                        }
                    case 4:
                        {
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            OpcaoInvalida();
                            break;
                        }

                }
                
                
            }
            catch (Exception)
            {
                OpcaoInvalida();
            }
        }

        void ListarAmigos()
        {
            Console.Clear();
            foreach (Pessoa pessoa in this.Pessoas)
                Console.WriteLine("Nome: {0} Latitude: {1} Longitude: {2}", 
                    pessoa.Nome, 
                    pessoa.Latitude, 
                    pessoa.Longitude);

            Console.WriteLine("Pressione Enter para retornar ao menu principal...");
            Console.ReadLine();
            MenuPrincipal();
        }

        void ListarAmigosProximos()
        {
            Console.Clear();
            Console.WriteLine("Informe o nome do amigo: ");
            string nome = Console.ReadLine();
            Pessoa amigo=this.Pessoas.Find(o => o.Nome == nome);

            if (amigo==null)
            {
                Console.Clear();
                Console.WriteLine("Amigo não encontrado na lista. Pressione Enter para retornar ao menu principal...");
                Console.ReadLine();
                MenuPrincipal();
                return;
            }

            if (amigo.AmigosProximos.Count==0)
            {
                Console.Clear();
                Console.WriteLine("Não possui amigos próximos. Pressione Enter para retornar ao menu principal...");
                Console.ReadLine();
                MenuPrincipal();
                return;
            }


            Console.Clear();
            foreach(Pessoa amigoProximo in amigo.AmigosProximos)
            {
                Console.WriteLine("Nome: {0} Latitude: {1} Longitude: {2}",
                    amigoProximo.Nome,
                    amigoProximo.Latitude,
                    amigoProximo.Longitude);

            }

            Console.WriteLine("Pressione Enter para retornar ao menu principal...");
            Console.ReadLine();
            MenuPrincipal();

        }

        void CadastrarAmigo()
        {
            Pessoa amigo = new Pessoa();

            Console.Clear();

            Console.WriteLine("Informe o nome: ");
            amigo.Nome = Console.ReadLine();

            Console.WriteLine("Informe a latitude: ");
            amigo.Latitude = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Informe a longitude: ");
            amigo.Longitude = Convert.ToInt32(Console.ReadLine());

            if (LocalDuplicado(amigo))
            {
                Console.Clear();
                Console.WriteLine("Local já habitado por outro amigo. Pressione qualquer tecla para continuar.");
                Console.ReadLine();
                MenuPrincipal();
            }

            Console.WriteLine("Informe os amigos próximos: ");
            
            for(int i = 0; i < 3; i++)
            {
                Console.WriteLine("Amigo número: {0}", i+1);
                
                Pessoa amigoProximo = new Pessoa();

                Console.WriteLine("Informe o nome: ");
                amigoProximo.Nome= Console.ReadLine();

                Console.WriteLine("Informe a latitude: ");
                amigoProximo.Latitude = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Informe a longitude: ");
                amigoProximo.Longitude = Convert.ToInt32(Console.ReadLine());

                if (amigo.AmigosProximos == null)
                    amigo.AmigosProximos = new List<Pessoa>();

                amigo.AmigosProximos.Add(amigoProximo);
            }
            
            this.Pessoas.Add(amigo);
            
            MenuPrincipal();
        }

        bool LocalDuplicado(Pessoa amigo)
        {
            Pessoa amigoLocalDuplicado=this.Pessoas.Find(o => o.Latitude == amigo.Latitude && 
                                                              o.Longitude == amigo.Longitude);
            return !(amigoLocalDuplicado == null);
        }

    }
}
