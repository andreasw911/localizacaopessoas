﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace LocalizacaoPessoas
{
    class Pessoa
    {
        string nome;
        int latitude;
        int longitude;
        List<Pessoa> amigosProximos;
        
        public string Nome {
            get
            {
                return this.nome;
            }
            set
            {
                this.nome = value;
            }
        }


        public int Latitude
        {
            get
            {
                return this.latitude;
            }

            set
            {
                this.latitude = value;
            }
        }

        public int Longitude
        {
            get
            {
                return this.longitude;
            }

            set
            {
                this.longitude = value;
            }
        }

        public List<Pessoa> AmigosProximos
        {
            get
            {
                return this.amigosProximos;
            }
            set
            {
                this.amigosProximos = value;
            }
        }
    }
}
